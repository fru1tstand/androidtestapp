package me.codeli.kodleetest;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View
{
	private long			previousTime	= 0;
	private static int		canvasWidth		= 0;
	private static int		canvasHeight	= 0;
	
	private static Paint	defaultPaint	= null;
	private static Paint	paint_title		= null;
	private static Paint	paint_ball		= null;
	
	ArrayList<Ball>			ballList		= new ArrayList<Ball>();
	
	public GameView(Context context) {
		super(context);
		
		if (GameView.paint_ball == null)
		{
			GameView.paint_ball = new Paint();
			GameView.paint_ball.setColor(Color.MAGENTA);
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		if (canvas.getWidth() != GameView.canvasWidth || canvas.getHeight() != GameView.canvasHeight)
		{
			GameView.canvasWidth = canvas.getWidth();
			GameView.canvasHeight = canvas.getHeight();
			GameView.updatePaints();
		}
		
		canvas.drawColor(Color.BLACK);
		
		// Random
		canvas.drawText("Hi! Crystal was here!", 50, 400, GameView.defaultPaint);
		
		// Balls
		for (Ball b : this.ballList)
			canvas.drawCircle(b.getPoint().x, b.getPoint().y, b.getSize(), GameView.paint_ball);
		
		// Title
		canvas.drawText(this.getContext().getString(R.string.app_name), canvas.getWidth() / 2 - GameView.paint_title.measureText(this.getContext().getString(R.string.app_name)) / 2, (int) (0.3 * canvas.getHeight()), GameView.paint_title);
		
		// FPS
		canvas.drawText("FPS: " + (1000 / (System.currentTimeMillis() - this.previousTime)), 50, canvas.getHeight() - 50, GameView.defaultPaint);
		this.previousTime = System.currentTimeMillis();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		this.ballList.add(new Ball(new Point((int) e.getX(), (int) e.getY()), (int) ((Math.random() + 1) * 10)));
		return true;
	}
	
	private static void updatePaints()
	{
		GameView.defaultPaint = new Paint();
		GameView.defaultPaint.setTextSize(20);
		GameView.defaultPaint.setColor(Color.CYAN);
		
		GameView.paint_title = new Paint();
		GameView.paint_title.setColor(Color.WHITE);
		GameView.paint_title.setTypeface(Typeface.create("Helvetica", Typeface.BOLD));
		GameView.paint_title.setTextSize(50);
	}
	
	private class Ball
	{
		Point	p		= null;
		int		size	= 0;
		
		public Ball(Point p, int size) {
			this.p = p;
			this.size = size;
		}
		
		public Point getPoint()
		{
			return this.p;
		}
		
		public int getSize()
		{
			return this.size;
		}
	}
}
